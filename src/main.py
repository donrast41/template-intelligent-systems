# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import argparse, json, os
import logging
from dummy_model import DummyNet
from prepare_dataset import prepare_dataset
import matplotlib.pyplot as plt

from train import train


def training_curve(scores, path):
    fig, ax = plt.subplots(1, 1)

    ax.plot(list(range(1, len(scores["train"]) + 1)), scores["train"])
    train_scores = ax.scatter(
        list(range(1, len(scores["train"]) + 1)), scores["train"],
    )

    ax.plot(list(range(1, len(scores["val"]) + 1)), scores["val"])
    val_scores = ax.scatter(
        list(range(1, len(scores["val"]) + 1)), scores["val"],
    )

    ax.set_xlabel("Number of iteration")
    ax.set_ylabel("Loss")

    ax.legend([train_scores, val_scores], ["Train loss", "Validation loss"])

    plt.tight_layout()
    plt.savefig(path, dpi=300)


def check_directory(directory):
    try:
        os.makedirs(directory)
    except FileExistsError:
        return None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_init_params")
    parser.add_argument("--data")
    parser.add_argument("--test_size")
    parser.add_argument("--n_epochs")

    args = parser.parse_args()
    model_init_params = json.loads(args.model_init_params)
    data, features_cols, target_col = prepare_dataset(args.data)

    test_size = float(args.test_size)
    n_epochs = int(args.n_epochs)

    net, scores_by_iter = train(
        DummyNet,
        model_init_params,
        data,
        target_col,
        features_cols,
        test_size,
        n_epochs
    )

    check_directory("./visualizations")
    training_curve(scores_by_iter, "visualizations/train_curve.png")

    logger = logging.getLogger("mldev")
    logger.setLevel(logging.INFO)
    sh = logging.StreamHandler()
    sh.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
    logger.addHandler(sh)

    # todo write a report
    logger.info(f"net={net}")
    logger.info(f"best train score: {min(scores_by_iter['train'])}")


if __name__ == "__main__":
    main()
