# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

from sklearn.model_selection import train_test_split
from tensorboard_logger import tensorboard_logger

import torch
import torch.nn as nn
from torch.autograd import Variable

import os, time


def train(model, model_init_params, data, target_col, features_cols, test_size, n_epochs):
    log_dir = os.environ.get('MLDEV_LOG_DIR', './logs/')
    run_name = os.environ.get('MLDEV_RUN_NAME', '')
    run_index = os.environ.get('MLDEV_RUN_INDEX', '')

    logger = tensorboard_logger.Logger(os.path.join(str(log_dir), str(run_name), str(run_index)),
                                       flush_secs=1)

    train_X, test_X, train_y, test_y = train_test_split(
        data[features_cols].values,
        data[target_col].values,
        test_size=test_size
    )
    train_X = Variable(torch.Tensor(train_X).float())
    test_X = Variable(torch.Tensor(test_X).float())
    train_y = Variable(torch.Tensor(train_y).long())
    test_y = Variable(torch.Tensor(test_y).long())

    net = model(**model_init_params)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=0.01)

    scores_by_iter = {
        "train": [],
        "val": [],
    }
    for epoch in range(n_epochs):
        optimizer.zero_grad()

        train_out = net(train_X)
        train_loss = criterion(train_out, train_y)

        with torch.no_grad():
            net.eval()
            test_out = net(test_X)
            test_loss = criterion(test_out, test_y)
            net.train()

        train_loss.backward()
        optimizer.step()

        scores_by_iter["train"].append(train_loss.data.item())
        scores_by_iter["val"].append(test_loss.data.item())

        logger.log_value(f"{run_name}/loss/train", scores_by_iter["train"][-1], step=epoch)
        logger.log_value(f"{run_name}/loss/val", scores_by_iter["val"][-1], step=epoch)

        if epoch % 100 == 0:
            print(f"number of epoch {epoch}, "
                  f"train loss {round(train_loss.data.item(), 4)}, "
                  f"test loss {round(test_loss.data.item(), 4)}")
    return net, scores_by_iter
