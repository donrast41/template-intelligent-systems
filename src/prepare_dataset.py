# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import pandas as pd


def prepare_dataset(path):
    data = pd.read_csv(path)
    data.loc[data["Species"] == "Iris-setosa", "Species"] = 0
    data.loc[data["Species"] == "Iris-versicolor", "Species"] = 1
    data.loc[data["Species"] == "Iris-virginica", "Species"] = 2
    data["Species"] = data["Species"].astype("int")

    target_col = "Species"
    features_cols = ["SepalLengthCm", "SepalWidthCm",
                     "PetalLengthCm", "PetalWidthCm"]

    return data, features_cols, target_col
