# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import os

import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag


@experiment_tag()
class SamplePowerAnalysis(object):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.num_iterations = kwargs["num_iterations"]
        self.train_fractions = kwargs["train_fractions"]

        self.train = kwargs["train"]
        self.prepare_dataset = kwargs["prepare_dataset"]
        self.model = kwargs["model"]

        self.dataset = kwargs["dataset"]
        self.outputs = kwargs["outputs"]
        self.model_init_params = kwargs["model_init_params"]
        self.n_epochs = kwargs["n_epochs"]

    @staticmethod
    def generate_report(results, orient, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        results_df = pd.DataFrame.from_dict(results, orient=orient)
        results_df.to_html(path)

    def __call__(self, *args, **kwargs):
        self.data, self.features_cols, self.target_col = self.prepare_dataset(
            path=str(self.dataset)
        )

        results = {
            "train_fraction": [],
            "train": [],
            "test": [],
        }

        for frac in self.train_fractions:
            print(f"train fraction is {frac}")
            scores_train, scores_test = [], []

            os.environ['MLDEV_RUN_NAME'] = 'SamplePowerAnalysis/' + str(frac)

            for i in range(self.num_iterations):
                print(f"\titeration #{i + 1}")

                os.environ['MLDEV_RUN_INDEX'] = str(i)

                model, scores = self.train(
                    model=self.model,
                    model_init_params=self.model_init_params,
                    data=self.data,
                    target_col=self.target_col,
                    features_cols=self.features_cols,
                    test_size=1 - frac,
                    n_epochs=self.n_epochs
                )
                scores_train.append(min(scores["train"]))
                scores_test.append(min(scores["val"]))

            results["train_fraction"] += [frac]
            results["train"] += [np.mean(scores_train)]
            results["test"] += [np.mean(scores_test)]

        self.generate_report(
            results=results,
            orient="columns",
            path=os.path.join(str(self.outputs), "report_sample_power_analysis.html")
        )
