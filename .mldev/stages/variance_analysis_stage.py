# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import os

import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag


@experiment_tag()
class VarianceAnalysis(object):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.num_iterations = kwargs["num_iterations"]
        self.train_fraction = kwargs["train_fraction"]

        self.train = kwargs["train"]
        self.prepare_dataset = kwargs["prepare_dataset"]
        self.model = kwargs["model"]

        self.dataset = kwargs["dataset"]
        self.outputs = kwargs["outputs"]
        self.model_init_params = kwargs["model_init_params"]
        self.n_epochs = kwargs["n_epochs"]

    @staticmethod
    def generate_report(results, orient, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        results_df = pd.DataFrame.from_dict(results, orient=orient)
        results_df.to_html(path)

    def __call__(self, *args, **kwargs):
        self.data, self.features_cols, self.target_col = self.prepare_dataset(
            path=str(self.dataset)
        )

        os.environ['MLDEV_RUN_NAME'] = 'VarianceAnalysis'

        scores_train, scores_test = [], []
        for i in range(self.num_iterations):
            print(f"iteration #{i + 1}")

            os.environ['MLDEV_RUN_INDEX'] = str(i)

            model, scores = self.train(
                model=self.model,
                model_init_params=self.model_init_params,
                data=self.data,
                target_col=self.target_col,
                features_cols=self.features_cols,
                test_size=1 - self.train_fraction,
                n_epochs=self.n_epochs
            )

            scores_train.append(min(scores["train"]))
            scores_test.append(min(scores["val"]))

        results = {
            "train": {
                "mean": np.mean(scores_train),
                "variance": np.var(scores_train),
            },
            "test": {
                "mean": np.mean(scores_test),
                "variance": np.var(scores_test),
            },
        }

        self.generate_report(
            results=results,
            orient="index",
            path=os.path.join(str(self.outputs), "report_variance_analysis.html")
        )
