# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import os

import pandas as pd
from pandas_profiling import ProfileReport

from mldev.experiment import experiment_tag

# todo rewrite
@experiment_tag
class EDA(object):

    def __init__(self, attributes={}, versioned_input_data={}, versioned_output_data={}):
        super().__init__()

        self.attributes = attributes
        self.versioned_input_data = versioned_input_data
        self.versioned_output_data = versioned_output_data

    def __repr__(self):
        return (
            "{}(attributes={}, versioned_input_data={}, versioned_output_data={})"
            .format(
                self.__class__.__name__,
                self.attributes,
                self.versioned_input_data,
                self.versioned_output_data,
            )
        )

    def __call__(self, *args, **kwargs):
        target_dir = self.versioned_output_data.get("visualizations_directory", "visualizations")
        os.makedirs(target_dir, exist_ok=True)
        self.build_report(target_dir)

    def build_report(self, directory):
        for file in self.versioned_input_data.get("input_files", []):
            data = pd.read_csv(file)
            (
                ProfileReport(
                    data,
                    title=self.attributes.get("title", "Exploratory analysis report"),
                    minimal=self.attributes.get("large_dataset", False)
                )
                .to_file(f"{directory}/{file.split('/')[-1].split('.')[0]}_eda_report.html")
            )

